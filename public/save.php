<?php
     $nom="nom";
     $msg="message";
     $mail="mail";
     $s_file = 'data/last_message.json';
  
     try {
         $s_fileData = file_get_contents($s_file);
          
         if( !$s_fileData || strlen($s_fileData) == 0 ) {
             $tableau_pour_json = array();
         } else {
             $tableau_pour_json = json_decode($s_fileData, true);
         }
         array_push( $tableau_pour_json, array(
             'nom' => $nom,
             'prenom' => $prenom,
             'message' => $msg,
             'mail' => $mail
         ));
         $contenu_json = json_encode($tableau_pour_json);
         file_put_contents($s_file, $contenu_json);
         echo "Vos informations ont été enregistrées";
     }
     catch( Exception $e ) {
         echo "Erreur : ".$e->getMessage();
     }
 ?>
