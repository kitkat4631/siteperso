<?php

/* 
vous ajouterez ici les fonctions qui vous sont utiles dans le site,
je vous ai créé la première qui est pour le moment incomplète et qui devra contenir
la logique pour choisir la page à charger
*/

function getContent(){
	if(!isset($_GET['page'])){
		include __DIR__.'/../pages/home.php';
	} elseif ($_GET['page'] == 'bio'){
		include __DIR__.'/../pages/bio.php';
	}
	elseif ($_GET['page'] == 'contact') {
		include __DIR__.'/../pages/contact.php';
	}
}

function getPart($name){
	include __DIR__ . '/../parts/'. $name . '.php';
}

function getUserData(){
$json = file_get_contents("../data/user.json");

echo $json;
var_dump(json_decode($json));
};

function __construct($file_name, $parse_header=false, $delimiter="\t", $length=8000)
    {
        $this->fp = fopen($file_name, "r");
        $this->parse_header = $parse_header;
        $this->delimiter = $delimiter;
        $this->length = $length;
        $this->lines = $lines;

        if ($this->parse_header)
        {
           $this->header = fgetjson($this->fp, $this->length, $this->delimiter);
        }

    } 